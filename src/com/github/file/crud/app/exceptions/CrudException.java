package com.github.file.crud.app.exceptions;

public class CrudException extends RuntimeException {

    public CrudException() {
    }

    public CrudException(String message) {
        super(message);
    }
}
