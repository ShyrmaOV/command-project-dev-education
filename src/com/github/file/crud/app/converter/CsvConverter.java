package com.github.file.crud.app.converter;

import java.util.*;

import com.github.file.crud.app.models.Person;

public class CsvConverter implements Converter {

    @Override
    public List<Person> fromStringDataToObjects(String file) {
        if (file == null || "".equals(file)) {
            return null;
        }
        List<Person> people = new ArrayList<>();
        String[] arrayStr;
        String[] insideArrayStr;
        arrayStr = file.split("\n");
        long id = 0;
        String firstName = "";
        String lastName = "";
        int age = 0;
        String city = "";
        for (int i = 0; i < arrayStr.length; i++) {
            insideArrayStr = arrayStr[i].split(",");
            for (int j = 0; j < insideArrayStr.length; j++) {
                switch (j) {
                    case (0):
                        id = Long.parseLong(insideArrayStr[j]);
                        break;
                    case (1):
                        firstName = insideArrayStr[j];
                        break;
                    case (2):
                        lastName = insideArrayStr[j];
                        break;
                    case (3):
                        age = Integer.parseInt(insideArrayStr[j]);
                        break;
                    case (4):
                        city = insideArrayStr[j];
                        break;
                    default:
                        break;
                }
            }
            Person person = new Person(id, firstName, lastName, age, city);
            people.add(person);
        }
        return people;

    }

    @Override
    public String toStringDataFromObjects(List<Person> people) {
        if (people == null || people.isEmpty()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (Person person : people) {
            stringBuilder.append(person.getId()).append(",");
            stringBuilder.append(person.getFirstName()).append(",");
            stringBuilder.append(person.getLastName()).append(",");
            stringBuilder.append(person.getAge()).append(",");
            stringBuilder.append(person.getCity());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
