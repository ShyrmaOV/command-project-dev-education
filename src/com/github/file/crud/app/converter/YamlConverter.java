package com.github.file.crud.app.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.file.crud.app.models.Person;

public class YamlConverter implements Converter {

    public List<Person> fromStringDataToObjects(String file) {

        List<Person> people = new ArrayList<>();

        long id = 0;
        String firstName = "";
        String lastName = "";
        int age = 0;
        String city = "";

        String[] arrayStr;
        String[] insideArrayStr;

        file = file.replaceAll(" ", "");
        file = file.replaceAll("\t", "");

        arrayStr = file.split("\n");

        for (int i = 0; i < arrayStr.length; i++) {
            insideArrayStr = arrayStr[i].split(":");
            int parserCode = (i);
            switch (parserCode) {
                case (0):
                    break;
                case (1):
                    id = Long.parseLong(insideArrayStr[1]);
                    break;
                case (2):
                    firstName = insideArrayStr[1];
                    break;
                case (3):
                    lastName = insideArrayStr[1];
                    break;
                case (4):
                    age = Integer.parseInt(insideArrayStr[1]);
                    break;
                case (5):
                    city = insideArrayStr[1];
                    break;
                default:
                    break;
            }
        }
        Person person = new Person(id, firstName, lastName, age, city);
        people.add(person);
        return people;
    }

    public String toStringDataFromObjects(List<Person> people) {
        String stream = "";
        int count = 1;
        for (int i = 0; i < people.size(); i++) {
            stream += "Person" + count++ + ": " + "\n";
            stream += " -id: " + people.get(i).getId() + "\n";
            stream += " -firstName: " + people.get(i).getFirstName() + "\n";
            stream += " -lastName: " + people.get(i).getLastName() + "\n";
            stream += " -age: " + people.get(i).getAge() + "\n";
            stream += " -city: " + people.get(i).getCity() + "\n";
        }
        return stream;
    }
}
