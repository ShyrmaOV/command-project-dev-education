package com.github.file.crud.app.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.github.file.crud.app.models.Person;

public interface Converter {

    List<Person> fromStringDataToObjects(String data);

    String toStringDataFromObjects(List<Person> persons);

    default String toStringDataFromObject(Person person) {
        List<Person> peoples = new ArrayList<>();
        peoples.add(person);
        return toStringDataFromObjects(peoples);
    }
}
