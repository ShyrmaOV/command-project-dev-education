package com.github.file.crud.app.utils;

import com.github.file.crud.app.exceptions.CrudException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {

    public static boolean isFileExist(String fullPathToFile) {
        return new File(fullPathToFile).isFile();
    }

    public static String readFileAsStringData(String fullPathToFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(fullPathToFile))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } catch(FileNotFoundException e) {
            throw new CrudException("Passed file name not found!");
        } catch(IOException e) {
            throw new CrudException(e.getMessage());
        }
    }

    public static File createFile(String fullPathToFile) {
        File newFile = new File(fullPathToFile);
        try {
            newFile.createNewFile();
            return newFile;
        } catch(IOException e) {
            throw new CrudException(e.getMessage());
        }
    }

    public static boolean writeStringDataToFile(File file, String data) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(data);
            return true;
        } catch(IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static boolean writeStringDataToFile(String fullPathToFile, String data) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fullPathToFile))) {
            writer.write(data);
            return true;
        } catch(IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
