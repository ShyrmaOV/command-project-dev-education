package com.github.file.crud.app.view;

import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.view.models.FileType;
import com.github.file.crud.app.view.models.MenuCommand;
import com.github.file.crud.app.view.models.MenuOperation;

import java.util.Scanner;

import static com.github.file.crud.app.dao.DaoFactory.getDaoByType;
import static com.github.file.crud.app.view.models.ConsoleColors.GREEN_BOLD;
import static com.github.file.crud.app.view.models.ConsoleColors.RESET;

public class MenuHelper {

    private static final Scanner scanner = new Scanner(System.in);

    public static MenuCommand parseMenuCommand(String commandAsString) {
        MenuCommand command = new MenuCommand();
        String[] menuOperationAfterSplit = commandAsString.trim().split(" ");

        if (menuOperationAfterSplit.length == 1) {
            MenuOperation menuOperation = MenuOperation.from(menuOperationAfterSplit[0]);
            if (menuOperation == null) {
                throw new CrudException("You enter incorrect command! Print help to see all available commands!");
            }
            command.setOperation(menuOperation);
            return command;
        } else if (menuOperationAfterSplit.length == 2) {
            MenuOperation menuOperation = MenuOperation.from(menuOperationAfterSplit[0]);
            if (menuOperation == null) {
                throw new CrudException("You enter incorrect CRUD command! Print help to see all available CRUD commands!");
            }

            String fullPathToFile = menuOperationAfterSplit[1];
            String[] fileNameWithExtension = fullPathToFile.split("\\.");
            if (fileNameWithExtension.length != 2) {
                throw new CrudException("You enter incorrect full path to file!");
            }

            FileType fileType = FileType.from(fileNameWithExtension[1]);

            if (fileType == null) {
                throw new CrudException("You enter incorrect file extension!");
            }

            command.setOperation(menuOperation);
            command.setFullPathToFile(fullPathToFile);
            command.setFileType(fileType);

            return command;
        } else {
            throw new CrudException("You enter incorrect command! Print help to see all available commands!");
        }
    }

    public static void processCommand(MenuCommand command) {
        try {
            switch (command.getOperation()) {
                case CREATE:
                    printCrudBoolAnswer(getDaoByType(command.getFileType())
                            .create(command.getFullPathToFile(), enterPersonDataWithId()));
                    break;
                case READ:
                    System.out.println(getDaoByType(command.getFileType())
                            .read(command.getFullPathToFile()));
                    break;
                case UPDATE:
                    printCrudBoolAnswer(getDaoByType(command.getFileType())
                            .update(command.getFullPathToFile(), enterPersonDataWithId()));
                    break;
                case DELETE:
                    printCrudBoolAnswer(getDaoByType(command.getFileType())
                            .delete(command.getFullPathToFile(), enterPersonId()));
                    break;
                case EXIT:
                    System.exit(0);
                    break;
                case HELP:
                    MenuHelper.printHelp();
                    break;
                case SWITCH:
                    System.out.println("SWITCH");
                    break;
            }
        } catch (CrudException e) {
            System.out.println("Your operation finished with exception! " + e.getMessage());
            System.out.println("Please try again!");
            throw e;
        }
    }

    protected static void printCrudBoolAnswer(boolean commandAnswer) {
        System.out.println(commandAnswer
                ? "CRUD command was finished successful."
                : "CRUD command was finished with error. Please try again.");
    }

    private static Person enterPersonDataWithId() {
        Person person = new Person();

        System.out.println("=== [Please enter Person data below] ===");
        System.out.print("\nID: ");
        person.setId(Long.parseLong(scanner.nextLine()));

        System.out.print("\nFirst name: ");
        person.setFirstName(scanner.nextLine());

        System.out.print("\nLast name: ");
        person.setLastName(scanner.nextLine());

        System.out.print("\nAge: ");
        person.setAge(Integer.parseInt(scanner.nextLine()));

        System.out.print("\nCity: ");
        person.setCity(scanner.nextLine());

        return person;
    }

    private static long enterPersonId() {
        System.out.print("\nID: ");
        return Long.parseLong(scanner.nextLine());
    }

    public static void printHelp() {
        System.out.println("Commands that the program supports:");
        System.out.println("\nCRUD commands:");
        System.out.println(GREEN_BOLD);
        System.out.println("create {full path to file with extension}");
        System.out.println("read {full path to file with extension}");
        System.out.println("update {full path to file with extension}");
        System.out.println("delete {full path to file with extension}");
        System.out.println(RESET);
        System.out.println("Other commands:");
        System.out.println(GREEN_BOLD);
        System.out.println("exit");
        System.out.println("help");
        System.out.println("switch");
        System.out.println(RESET);
    }
}
