package com.github.file.crud.app.view;

import java.util.Scanner;

import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.view.models.MenuCommand;

public class ConsoleMenuCrudApplication implements MenuCrudApplication {

    @Override
    public void runMenu() {
        Scanner scanner = new Scanner(System. in);

        MenuHelper.printHelp();
        while (true) {
            System.out.print("\nEnter the command: ");
            try {
                MenuCommand command = MenuHelper.parseMenuCommand(scanner.nextLine());
                MenuHelper.processCommand(command);
            } catch(CrudException e) {
                System.out.println("Your command is incorrect! " + e.getMessage());
                System.out.println("Please try to enter again!");
            }
        }
    }
}
