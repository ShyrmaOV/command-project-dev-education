package com.github.file.crud.app.view.models;

public enum MenuOperation {
    CREATE,
    READ,
    UPDATE,
    DELETE,
    EXIT,
    HELP,
    SWITCH;

    public static MenuOperation from(String name) {
        for (MenuOperation operation : MenuOperation.values()) {
            if (operation.name().equalsIgnoreCase(name)) {
                return operation;
            }
        }
        return null;
    }
}
