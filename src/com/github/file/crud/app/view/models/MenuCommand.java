package com.github.file.crud.app.view.models;

public class MenuCommand {

    private MenuOperation operation;
    private String fullPathToFile;
    private FileType fileType;


    public MenuCommand() {
    }

    public MenuCommand(MenuOperation operation, String fullPathToFile, FileType fileType) {
        this.operation = operation;
        this.fullPathToFile = fullPathToFile;
        this.fileType = fileType;
    }

    public MenuOperation getOperation() {
        return operation;
    }

    public void setOperation(MenuOperation operation) {
        this.operation = operation;
    }

    public String getFullPathToFile() {
        return fullPathToFile;
    }

    public void setFullPathToFile(String fullPathToFile) {
        this.fullPathToFile = fullPathToFile;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }
}
