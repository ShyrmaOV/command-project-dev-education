package com.github.file.crud.app.view.models;

public enum FileType {
    BIN,
    JSON,
    CSV,
    XML,
    YAML;

    public static FileType from(String name) {
        for (FileType type : FileType.values()) {
            if (type.name().equalsIgnoreCase(name)) {
                return type;
            }
        }
        return null;
    }
}
