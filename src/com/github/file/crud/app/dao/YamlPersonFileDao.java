package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.YamlConverter;

public class YamlPersonFileDao extends AbstractPersonDao {

    public YamlPersonFileDao() {
        super(new YamlConverter());
    }
}
