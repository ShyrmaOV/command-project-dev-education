package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.BinConverter;

public class BinPersonFileDao extends AbstractPersonDao {

    public BinPersonFileDao() {
        super(new BinConverter());
    }
}
