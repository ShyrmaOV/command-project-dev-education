package com.github.file.crud.app.dao;

import java.util.List;
import java.util.UUID;

import com.github.file.crud.app.models.Person;

public interface PersonFileDao {

    boolean create(String fullPathToFile, Person person);
    Person read(String fullPathToFile, long id);
    List<Person> read(String fullPathToFile);
    boolean update(String fullPathToFile, Person person);
    boolean delete(String fullPathToFile, long personId);
}
