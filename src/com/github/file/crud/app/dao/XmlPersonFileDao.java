package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.XmlConverter;

public class XmlPersonFileDao extends AbstractPersonDao {

    public XmlPersonFileDao() {super(new XmlConverter());}
}
