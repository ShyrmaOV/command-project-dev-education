package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.CsvConverter;

public class CsvPersonFileDao extends AbstractPersonDao {

    public CsvPersonFileDao() {
        super(new CsvConverter());
    }
}
