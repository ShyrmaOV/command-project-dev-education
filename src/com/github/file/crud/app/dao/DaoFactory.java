package com.github.file.crud.app.dao;

import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.view.models.FileType;

import java.util.HashMap;
import java.util.Map;

public class DaoFactory {

    private static final Map<FileType, PersonFileDao> daosByTypes = new HashMap<>();

    static {
        daosByTypes.put(FileType.BIN, new BinPersonFileDao());
        daosByTypes.put(FileType.JSON, new JsonPersonFileDao());
        daosByTypes.put(FileType.CSV, new CsvPersonFileDao());
        daosByTypes.put(FileType.XML, new XmlPersonFileDao());
        daosByTypes.put(FileType.YAML, new YamlPersonFileDao());
    }

    public static PersonFileDao getDaoByType(FileType fileType) {
        for (Map.Entry<FileType, PersonFileDao> entryTypeDao : daosByTypes.entrySet()) {
            if (entryTypeDao.getKey().equals(fileType)) {
                return entryTypeDao.getValue();
            }
        }
        throw new CrudException("Not found DAO from file type: " + fileType);
    }
}
