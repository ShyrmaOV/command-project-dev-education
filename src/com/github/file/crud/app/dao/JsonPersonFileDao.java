package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.JsonConverter;

public class JsonPersonFileDao extends AbstractPersonDao {

    public JsonPersonFileDao() {super(new JsonConverter());}
}
