package com.github.file.crud.app.test;

import com.github.file.crud.app.converter.BinConverter;
import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.utils.FileUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BinConverterTest {

    private static final String PATH_TO_BIN_FILE = "src/test/com/github/file/crud/app/converter/binExample.bin";

    @Test
    public void fromStringDataToObjects() {
        List<Person> expectedPersons = providePersons();
        List<Person> actualPersons = new BinConverter().fromStringDataToObjects(FileUtil.readFileAsStringData(PATH_TO_BIN_FILE));

        assertEquals(expectedPersons, actualPersons);
    }

    @Test
    public void toStringDataFromObjects() {
        List<Person> expectedPersons = providePersons();
        List<Person> actualPersons =
                new BinConverter().fromStringDataToObjects(new BinConverter().toStringDataFromObjects(providePersons()));

        assertEquals(expectedPersons, actualPersons);
    }

    private List<Person> providePersons() {
        return Arrays.asList(
                new Person(101, "fname 1", "lname 1", 1, "city 1"),
                new Person(102, "fname 2", "lname 2", 2, "city 2"),
                new Person(103, "fname 3", "lname 3", 3, "city 3")
        );
    }
}