package com.github.file.crud.app.test;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.CsvConverter;
import com.github.file.crud.app.models.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CsvConverterTest {

    private Converter format = new CsvConverter();

    @Test
    public void toStringDataFromObjects() {
        String exp = "1,Vasya,Pupkin,33,Odessa\n" + "2,Vasya,Pupkin,33,Odessa\n"
                + "3,Vasya,Pupkin,33,Odessa\n" + "4,Vasya,Pupkin,33,Odessa\n"
                + "5,Vasya,Pupkin,33,Odessa\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(2L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(3L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(4L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(5L, "Vasya", "Pupkin", 33, "Odessa"));
        String act = this.format.toStringDataFromObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsNull() {
        String act = this.format.toStringDataFromObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void toStringDataFromObjectsEmpty() {
        String act = this.format.toStringDataFromObjects(new ArrayList<>());
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(2L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(3L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(4L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(5L, "Vasya", "Pupkin", 33, "Odessa"));

        String data = "1,Vasya,Pupkin,33,Odessa\n" + "2,Vasya,Pupkin,33,Odessa\n"
                + "3,Vasya,Pupkin,33,Odessa\n" + "4,Vasya,Pupkin,33,Odessa\n"
                + "5,Vasya,Pupkin,33,Odessa\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsNull() {
        List<Person> act = this.format.fromStringDataToObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjectsEmpty() {
        String act = this.format.toStringDataFromObjects(new ArrayList<>());
        Assert.assertNull(act);
    }
}
