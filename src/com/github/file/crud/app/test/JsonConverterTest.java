package com.github.file.crud.app.test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.github.file.crud.app.converter.JsonConverter;
import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.utils.FileUtil;

import static org.junit.Assert.*;

public class JsonConverterTest {

    private static final String PATH_TO_JSON_FILE = "src/test/com/github/file/crud/app/converter/jsonExample.json";

    @org.junit.Test
    public void fromStringDataToObjects() {
        List<Person> expectedPersons = providePersons();
        List<Person> actualPersons = new JsonConverter().fromStringDataToObjects(FileUtil.readFileAsStringData(PATH_TO_JSON_FILE));

        assertEquals(expectedPersons, actualPersons);
    }

    @org.junit.Test
    public void toStringDataFromObjects() {
        assertEquals(
                FileUtil.readFileAsStringData(PATH_TO_JSON_FILE),
                new JsonConverter().toStringDataFromObjects(providePersons())
        );

    }

    private List<Person> providePersons() {
        return Arrays.asList(
                new Person(101, "fname 1", "lname 1", 1, "city 1"),
                new Person(102, "fname 2", "lname 2", 2, "city 2"),
                new Person(103, "fname 3", "lname 3", 3, "city 3")
        );
    }
}