package com.github.file.crud.app.test;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.YamlConverter;
import com.github.file.crud.app.models.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class YamlConverterTest {
    private Converter format = new YamlConverter();

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        String data = "Person: " + "\n" +
                "\tid: " + "1" + "\n" + "\tfirstName: " + "Alex" + "\n" + "\tlastName: " + "Ddl" + "\n" +
                "\tage: " + "22" + "\n" + "\tcity: " + "Odessa" + "\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }
}
